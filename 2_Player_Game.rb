require 'pry'
require 'colorize'
# 2 Player Game 
@current_user = @player2_name
@player1_lives = 3
@player2_lives = 3
@player1_score = 0
@player2_score = 0

def start_game
  # generate_question(@player1_name)
  # binding.pry
end

def alternate_questions(validated_answer)
  if validated_answer
  @current_user == @player1_name ? @player1_score += 1 : @player2_score += 1
  else
  @current_user == @player1_name ? @player1_lives -= 1 : @player2_lives -= 1
  update_score(@player1_lives,@player2_lives)
  end
alternate_players
end


def get_info
  puts "Player 1 enter your name : "
  @player1_name = gets.chomp
  puts "Player 2 enter your name : "
  @player2_name = gets.chomp
  puts "Let's start the game"
  alternate_players
end

def generate_question(player)
  first_random_number = rand(1..20)
  second_random_number = rand(1..20)  
  puts "#{player} : What does #{first_random_number} plus #{second_random_number} equal ?"
  answer_value = gets.chomp.to_i
  validate_answer(first_random_number,second_random_number,answer_value)
end

def validate_answer(fnumber,snumber,answer)
  @validated_answer = fnumber + snumber == answer ? true : false
  alternate_questions(@validated_answer)
end

def update_score(player1,player2)
puts "---------Player1-----------|-----------Player2------------".colorize(:brown)
puts "        #{player1}                                     #{player2}".colorize(:red)
end

def alternate_players
  if @current_user == @player1_name 
    @current_user = @player2_name
  else
    @current_user = @player1_name
  end

if (@player1_lives == 0 || @player2_lives == 0)
   @player1_lives > @player2_lives ?  (puts "Winner is #{@player1_name} and the score is #{@player1_lives}".colorize(:red)) :  (puts "Winner is #{@player2_name} and the score is #{@player2_lives}".colorize(:red))
else
  generate_question(@current_user)
end 

end


get_info
